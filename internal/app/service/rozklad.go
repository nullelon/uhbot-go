package service

import "uhbot/internal/ent"

type Rozklad interface {
	CurrentPairTeacher() (string, error)
}

type Links interface {
	ByTeacher(teacher string) ([]*ent.Link, error)
}

type Polls interface {
	Last() (*ent.Poll, error)
	All() ([]*ent.Poll, error)
	ById(id int) (*ent.Poll, error)
	Create(question string, answers []string, responses []string) (*ent.Poll, error)
	AllAnswersOnPoll(id int) ([]*ent.PollAnswer, error)
	SetAnswersOnPoll(pollId, studentId, answerId int) (*ent.PollStudentAnswer, error)
}

type Students interface {
	ByTelegramId(tgId int) (*ent.Student, error)
	All() ([]*ent.Student, error)
}

type Queue interface {
	ById(queueId int) (*ent.Queue, error)
	Create(objective string, tgMessageId int, entities []*ent.QueueEntity) (*ent.Queue, error)
	TeamsBySubject(subjectName string) ([]*ent.Team, error)
	Last() (*ent.Queue, error)
	ByTelegramMessageID(tgId int) (*ent.Queue, error)
	DeleteEntity(entityId int) error
}
