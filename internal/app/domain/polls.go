package domain

import (
	"context"
	"uhbot/internal/ent"
	"uhbot/internal/ent/poll"
	"uhbot/internal/ent/pollanswer"
)

type PollsService struct {
	ent *ent.Client
}

func NewPolls(ent *ent.Client) *PollsService {
	return &PollsService{ent: ent}
}

func (p PollsService) All() ([]*ent.Poll, error) {
	return p.ent.Poll.Query().All(context.Background())
}

func (p PollsService) Last() (*ent.Poll, error) {
	return p.ent.Poll.Query().
		Limit(1).
		WithPossibleAnswers(
			func(query *ent.PollAnswerQuery) {
				query.WithStudentAnswers(func(query *ent.PollStudentAnswerQuery) {
					query.WithAnswer()
				})
			},
		).
		WithStudentAnswers(func(q *ent.PollStudentAnswerQuery) {
			q.WithStudent()
			q.WithAnswer()
		}).
		Order(ent.Desc(poll.FieldID)).
		First(context.Background())
}

func (p PollsService) ById(id int) (*ent.Poll, error) {
	return p.ent.Poll.Query().
		Where(poll.ID(id)).
		WithPossibleAnswers().
		WithStudentAnswers(func(q *ent.PollStudentAnswerQuery) {
			q.WithStudent()
			q.WithAnswer()
		}).
		Only(context.Background())
}

func (p PollsService) Create(question string, answers []string, responses []string) (*ent.Poll, error) {
	pollAnswers := make([]*ent.PollAnswerCreate, len(answers))

	for i := 0; i < len(answers); i++ {
		pollAnswers[i] = p.ent.PollAnswer.Create().
			SetText(answers[i]).
			SetResponse(responses[i])
	}
	answersSaved, err := p.ent.PollAnswer.CreateBulk(pollAnswers...).Save(context.Background())
	if err != nil {
		return nil, err
	}

	newPoll, err := p.ent.Poll.
		Create().
		SetQuestion(question).
		AddPossibleAnswers(answersSaved...).
		Save(context.Background())
	if err != nil {
		return nil, err
	}

	newPoll.Edges.PossibleAnswers = answersSaved
	return newPoll, nil
}

func (p PollsService) AllAnswersOnPoll(id int) ([]*ent.PollAnswer, error) {
	return p.ent.PollAnswer.Query().
		Where(pollanswer.HasPollWith(poll.ID(id))).
		All(context.Background())
}

func (p PollsService) SetAnswersOnPoll(pollId, studentId, answerId int) (*ent.PollStudentAnswer, error) {
	return p.ent.PollStudentAnswer.Create().
		SetAnswerID(answerId).
		SetStudentID(studentId).
		SetPollID(pollId).Save(context.Background())
}
