package domain

import (
	"context"
	"uhbot/internal/ent"
	"uhbot/internal/ent/link"
)

type LinksService struct {
	ent *ent.Client
}

func NewLinks(ent *ent.Client) *LinksService {
	return &LinksService{ent: ent}
}

func (l LinksService) ByTeacher(teacher string) ([]*ent.Link, error) {
	return l.ent.Link.
		Query().
		Where(link.TeacherContains(teacher)).
		All(context.Background())
}
