package domain

import (
	"context"
	"uhbot/internal/ent"
	"uhbot/internal/ent/student"
)

type StudentsService struct {
	ent *ent.Client
}

func NewStudents(ent *ent.Client) *StudentsService {
	return &StudentsService{ent: ent}
}

func (s StudentsService) ByTelegramId(tgId int) (*ent.Student, error) {
	return s.ent.Student.Query().Where(student.TelegramID(tgId)).Only(context.Background())
}

func (s StudentsService) All() ([]*ent.Student, error) {
	return s.ent.Student.Query().
		WithAnswers().
		All(context.Background())
}
