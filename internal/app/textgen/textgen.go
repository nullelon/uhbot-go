package textgen

import (
	"encoding/json"
	"fmt"
	"github.com/mb-14/gomarkov"
	"io/ioutil"
	"strings"
)

var loadedModels = make(map[string]*gomarkov.Chain)
var models = make([]string, 0)

func LoadModelsFrom(folder string) error {
	loadedModels = make(map[string]*gomarkov.Chain)
	models = make([]string, 0)

	files, err := ioutil.ReadDir(folder)
	if err != nil {
		return err
	}

	for _, file := range files {
		if strings.HasSuffix(file.Name(), ".json") {
			_, err := LoadModel(folder + file.Name())
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func IsModelLoaded(modelName string) bool {
	_, ok := loadedModels[modelName]
	return ok
}

func ModelsList() []string {
	return models
}

func LoadModel(filename string) (*gomarkov.Chain, error) {
	if model, ok := loadedModels[filename]; ok {
		return model, nil
	}

	fmt.Println("loading model", filename)

	var chain gomarkov.Chain

	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &chain)
	if err != nil {
		return nil, err
	}

	filenameLevels := strings.Split(filename, "/")

	filename = filenameLevels[len(filenameLevels)-1]

	modelName := strings.ReplaceAll(filename, ".json", "")

	loadedModels[modelName] = &chain
	models = append(models, modelName)
	return &chain, nil
}

func Generate(modelName string) string {
	chain, ok := loadedModels[modelName]
	if !ok {
		return ""
	}

	tokens := []string{gomarkov.StartToken}
	for tokens[len(tokens)-1] != gomarkov.EndToken {
		next, _ := chain.Generate(tokens[len(tokens)-1:])
		tokens = append(tokens, next)
	}

	text := strings.TrimSpace(strings.Join(tokens[1:len(tokens)-1], " "))
	if text == "" {
		return Generate(modelName)
	}
	return text

}
