package app

import (
	tb "gopkg.in/tucnak/telebot.v2"
	"strings"
	"time"
)

func (b Bot) schedule() func(m *tb.Message) {
	return func(m *tb.Message) {
		lines := []string{
			"1 пара  08:30 - 10:05",
			"2 пара  10:25 - 12:00",
			"3 пара  12:20 - 13:55",
			"4 пара  14:15 - 15:50",
			"5 пара  16:10 - 17:45",
		}

		loc, _ := time.LoadLocation("Europe/Kiev")
		now := time.Now().In(loc)

		minutes := now.Hour()*60 + now.Minute()

		if minutes > 510 && minutes < 970+90 {
			pair := (minutes - 510) / 115
			lines[pair] = "<u>" + lines[pair] + "</u>"
		}

		b.tg.Send(m.Chat, strings.Join(lines, "\n"))
	}
}
