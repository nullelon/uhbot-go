package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
)

// PollStudentAnswer holds the schema definition for the PollStudentAnswer entity.
type PollStudentAnswer struct {
	ent.Schema
}

// Fields of the PollStudentAnswer.
func (PollStudentAnswer) Fields() []ent.Field {
	return nil
}

// Edges of the PollStudentAnswer.
func (PollStudentAnswer) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("poll", Poll.Type).Ref("studentAnswers").Unique(),
		edge.From("student", Student.Type).Ref("answers").Unique(),
		edge.From("answer", PollAnswer.Type).Ref("studentAnswers").Unique(),
	}
}
