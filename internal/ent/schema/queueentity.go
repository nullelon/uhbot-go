package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// QueueEntity holds the schema definition for the QueueEntity entity.
type QueueEntity struct {
	ent.Schema
}

// Fields of the QueueEntity.
func (QueueEntity) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").NotEmpty(),
		field.Int("position"),
	}
}

// Edges of the QueueEntity.
func (QueueEntity) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("queue", Queue.Type).Ref("all_entities"),
	}
}
