// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"fmt"
	"uhbot/internal/ent/poll"
	"uhbot/internal/ent/pollanswer"
	"uhbot/internal/ent/pollstudentanswer"
	"uhbot/internal/ent/predicate"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// PollUpdate is the builder for updating Poll entities.
type PollUpdate struct {
	config
	hooks    []Hook
	mutation *PollMutation
}

// Where adds a new predicate for the PollUpdate builder.
func (pu *PollUpdate) Where(ps ...predicate.Poll) *PollUpdate {
	pu.mutation.predicates = append(pu.mutation.predicates, ps...)
	return pu
}

// SetQuestion sets the "question" field.
func (pu *PollUpdate) SetQuestion(s string) *PollUpdate {
	pu.mutation.SetQuestion(s)
	return pu
}

// AddPossibleAnswerIDs adds the "possibleAnswers" edge to the PollAnswer entity by IDs.
func (pu *PollUpdate) AddPossibleAnswerIDs(ids ...int) *PollUpdate {
	pu.mutation.AddPossibleAnswerIDs(ids...)
	return pu
}

// AddPossibleAnswers adds the "possibleAnswers" edges to the PollAnswer entity.
func (pu *PollUpdate) AddPossibleAnswers(p ...*PollAnswer) *PollUpdate {
	ids := make([]int, len(p))
	for i := range p {
		ids[i] = p[i].ID
	}
	return pu.AddPossibleAnswerIDs(ids...)
}

// AddStudentAnswerIDs adds the "studentAnswers" edge to the PollStudentAnswer entity by IDs.
func (pu *PollUpdate) AddStudentAnswerIDs(ids ...int) *PollUpdate {
	pu.mutation.AddStudentAnswerIDs(ids...)
	return pu
}

// AddStudentAnswers adds the "studentAnswers" edges to the PollStudentAnswer entity.
func (pu *PollUpdate) AddStudentAnswers(p ...*PollStudentAnswer) *PollUpdate {
	ids := make([]int, len(p))
	for i := range p {
		ids[i] = p[i].ID
	}
	return pu.AddStudentAnswerIDs(ids...)
}

// Mutation returns the PollMutation object of the builder.
func (pu *PollUpdate) Mutation() *PollMutation {
	return pu.mutation
}

// ClearPossibleAnswers clears all "possibleAnswers" edges to the PollAnswer entity.
func (pu *PollUpdate) ClearPossibleAnswers() *PollUpdate {
	pu.mutation.ClearPossibleAnswers()
	return pu
}

// RemovePossibleAnswerIDs removes the "possibleAnswers" edge to PollAnswer entities by IDs.
func (pu *PollUpdate) RemovePossibleAnswerIDs(ids ...int) *PollUpdate {
	pu.mutation.RemovePossibleAnswerIDs(ids...)
	return pu
}

// RemovePossibleAnswers removes "possibleAnswers" edges to PollAnswer entities.
func (pu *PollUpdate) RemovePossibleAnswers(p ...*PollAnswer) *PollUpdate {
	ids := make([]int, len(p))
	for i := range p {
		ids[i] = p[i].ID
	}
	return pu.RemovePossibleAnswerIDs(ids...)
}

// ClearStudentAnswers clears all "studentAnswers" edges to the PollStudentAnswer entity.
func (pu *PollUpdate) ClearStudentAnswers() *PollUpdate {
	pu.mutation.ClearStudentAnswers()
	return pu
}

// RemoveStudentAnswerIDs removes the "studentAnswers" edge to PollStudentAnswer entities by IDs.
func (pu *PollUpdate) RemoveStudentAnswerIDs(ids ...int) *PollUpdate {
	pu.mutation.RemoveStudentAnswerIDs(ids...)
	return pu
}

// RemoveStudentAnswers removes "studentAnswers" edges to PollStudentAnswer entities.
func (pu *PollUpdate) RemoveStudentAnswers(p ...*PollStudentAnswer) *PollUpdate {
	ids := make([]int, len(p))
	for i := range p {
		ids[i] = p[i].ID
	}
	return pu.RemoveStudentAnswerIDs(ids...)
}

// Save executes the query and returns the number of nodes affected by the update operation.
func (pu *PollUpdate) Save(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(pu.hooks) == 0 {
		if err = pu.check(); err != nil {
			return 0, err
		}
		affected, err = pu.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*PollMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = pu.check(); err != nil {
				return 0, err
			}
			pu.mutation = mutation
			affected, err = pu.sqlSave(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(pu.hooks) - 1; i >= 0; i-- {
			mut = pu.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, pu.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// SaveX is like Save, but panics if an error occurs.
func (pu *PollUpdate) SaveX(ctx context.Context) int {
	affected, err := pu.Save(ctx)
	if err != nil {
		panic(err)
	}
	return affected
}

// Exec executes the query.
func (pu *PollUpdate) Exec(ctx context.Context) error {
	_, err := pu.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (pu *PollUpdate) ExecX(ctx context.Context) {
	if err := pu.Exec(ctx); err != nil {
		panic(err)
	}
}

// check runs all checks and user-defined validators on the builder.
func (pu *PollUpdate) check() error {
	if v, ok := pu.mutation.Question(); ok {
		if err := poll.QuestionValidator(v); err != nil {
			return &ValidationError{Name: "question", err: fmt.Errorf("ent: validator failed for field \"question\": %w", err)}
		}
	}
	return nil
}

func (pu *PollUpdate) sqlSave(ctx context.Context) (n int, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   poll.Table,
			Columns: poll.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: poll.FieldID,
			},
		},
	}
	if ps := pu.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := pu.mutation.Question(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: poll.FieldQuestion,
		})
	}
	if pu.mutation.PossibleAnswersCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: false,
			Table:   poll.PossibleAnswersTable,
			Columns: poll.PossibleAnswersPrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollanswer.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := pu.mutation.RemovedPossibleAnswersIDs(); len(nodes) > 0 && !pu.mutation.PossibleAnswersCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: false,
			Table:   poll.PossibleAnswersTable,
			Columns: poll.PossibleAnswersPrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollanswer.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := pu.mutation.PossibleAnswersIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: false,
			Table:   poll.PossibleAnswersTable,
			Columns: poll.PossibleAnswersPrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollanswer.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	if pu.mutation.StudentAnswersCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2M,
			Inverse: false,
			Table:   poll.StudentAnswersTable,
			Columns: []string{poll.StudentAnswersColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollstudentanswer.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := pu.mutation.RemovedStudentAnswersIDs(); len(nodes) > 0 && !pu.mutation.StudentAnswersCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2M,
			Inverse: false,
			Table:   poll.StudentAnswersTable,
			Columns: []string{poll.StudentAnswersColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollstudentanswer.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := pu.mutation.StudentAnswersIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2M,
			Inverse: false,
			Table:   poll.StudentAnswersTable,
			Columns: []string{poll.StudentAnswersColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollstudentanswer.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	if n, err = sqlgraph.UpdateNodes(ctx, pu.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{poll.Label}
		} else if cerr, ok := isSQLConstraintError(err); ok {
			err = cerr
		}
		return 0, err
	}
	return n, nil
}

// PollUpdateOne is the builder for updating a single Poll entity.
type PollUpdateOne struct {
	config
	fields   []string
	hooks    []Hook
	mutation *PollMutation
}

// SetQuestion sets the "question" field.
func (puo *PollUpdateOne) SetQuestion(s string) *PollUpdateOne {
	puo.mutation.SetQuestion(s)
	return puo
}

// AddPossibleAnswerIDs adds the "possibleAnswers" edge to the PollAnswer entity by IDs.
func (puo *PollUpdateOne) AddPossibleAnswerIDs(ids ...int) *PollUpdateOne {
	puo.mutation.AddPossibleAnswerIDs(ids...)
	return puo
}

// AddPossibleAnswers adds the "possibleAnswers" edges to the PollAnswer entity.
func (puo *PollUpdateOne) AddPossibleAnswers(p ...*PollAnswer) *PollUpdateOne {
	ids := make([]int, len(p))
	for i := range p {
		ids[i] = p[i].ID
	}
	return puo.AddPossibleAnswerIDs(ids...)
}

// AddStudentAnswerIDs adds the "studentAnswers" edge to the PollStudentAnswer entity by IDs.
func (puo *PollUpdateOne) AddStudentAnswerIDs(ids ...int) *PollUpdateOne {
	puo.mutation.AddStudentAnswerIDs(ids...)
	return puo
}

// AddStudentAnswers adds the "studentAnswers" edges to the PollStudentAnswer entity.
func (puo *PollUpdateOne) AddStudentAnswers(p ...*PollStudentAnswer) *PollUpdateOne {
	ids := make([]int, len(p))
	for i := range p {
		ids[i] = p[i].ID
	}
	return puo.AddStudentAnswerIDs(ids...)
}

// Mutation returns the PollMutation object of the builder.
func (puo *PollUpdateOne) Mutation() *PollMutation {
	return puo.mutation
}

// ClearPossibleAnswers clears all "possibleAnswers" edges to the PollAnswer entity.
func (puo *PollUpdateOne) ClearPossibleAnswers() *PollUpdateOne {
	puo.mutation.ClearPossibleAnswers()
	return puo
}

// RemovePossibleAnswerIDs removes the "possibleAnswers" edge to PollAnswer entities by IDs.
func (puo *PollUpdateOne) RemovePossibleAnswerIDs(ids ...int) *PollUpdateOne {
	puo.mutation.RemovePossibleAnswerIDs(ids...)
	return puo
}

// RemovePossibleAnswers removes "possibleAnswers" edges to PollAnswer entities.
func (puo *PollUpdateOne) RemovePossibleAnswers(p ...*PollAnswer) *PollUpdateOne {
	ids := make([]int, len(p))
	for i := range p {
		ids[i] = p[i].ID
	}
	return puo.RemovePossibleAnswerIDs(ids...)
}

// ClearStudentAnswers clears all "studentAnswers" edges to the PollStudentAnswer entity.
func (puo *PollUpdateOne) ClearStudentAnswers() *PollUpdateOne {
	puo.mutation.ClearStudentAnswers()
	return puo
}

// RemoveStudentAnswerIDs removes the "studentAnswers" edge to PollStudentAnswer entities by IDs.
func (puo *PollUpdateOne) RemoveStudentAnswerIDs(ids ...int) *PollUpdateOne {
	puo.mutation.RemoveStudentAnswerIDs(ids...)
	return puo
}

// RemoveStudentAnswers removes "studentAnswers" edges to PollStudentAnswer entities.
func (puo *PollUpdateOne) RemoveStudentAnswers(p ...*PollStudentAnswer) *PollUpdateOne {
	ids := make([]int, len(p))
	for i := range p {
		ids[i] = p[i].ID
	}
	return puo.RemoveStudentAnswerIDs(ids...)
}

// Select allows selecting one or more fields (columns) of the returned entity.
// The default is selecting all fields defined in the entity schema.
func (puo *PollUpdateOne) Select(field string, fields ...string) *PollUpdateOne {
	puo.fields = append([]string{field}, fields...)
	return puo
}

// Save executes the query and returns the updated Poll entity.
func (puo *PollUpdateOne) Save(ctx context.Context) (*Poll, error) {
	var (
		err  error
		node *Poll
	)
	if len(puo.hooks) == 0 {
		if err = puo.check(); err != nil {
			return nil, err
		}
		node, err = puo.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*PollMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = puo.check(); err != nil {
				return nil, err
			}
			puo.mutation = mutation
			node, err = puo.sqlSave(ctx)
			mutation.done = true
			return node, err
		})
		for i := len(puo.hooks) - 1; i >= 0; i-- {
			mut = puo.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, puo.mutation); err != nil {
			return nil, err
		}
	}
	return node, err
}

// SaveX is like Save, but panics if an error occurs.
func (puo *PollUpdateOne) SaveX(ctx context.Context) *Poll {
	node, err := puo.Save(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// Exec executes the query on the entity.
func (puo *PollUpdateOne) Exec(ctx context.Context) error {
	_, err := puo.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (puo *PollUpdateOne) ExecX(ctx context.Context) {
	if err := puo.Exec(ctx); err != nil {
		panic(err)
	}
}

// check runs all checks and user-defined validators on the builder.
func (puo *PollUpdateOne) check() error {
	if v, ok := puo.mutation.Question(); ok {
		if err := poll.QuestionValidator(v); err != nil {
			return &ValidationError{Name: "question", err: fmt.Errorf("ent: validator failed for field \"question\": %w", err)}
		}
	}
	return nil
}

func (puo *PollUpdateOne) sqlSave(ctx context.Context) (_node *Poll, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   poll.Table,
			Columns: poll.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: poll.FieldID,
			},
		},
	}
	id, ok := puo.mutation.ID()
	if !ok {
		return nil, &ValidationError{Name: "ID", err: fmt.Errorf("missing Poll.ID for update")}
	}
	_spec.Node.ID.Value = id
	if fields := puo.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, poll.FieldID)
		for _, f := range fields {
			if !poll.ValidColumn(f) {
				return nil, &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
			}
			if f != poll.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, f)
			}
		}
	}
	if ps := puo.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := puo.mutation.Question(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: poll.FieldQuestion,
		})
	}
	if puo.mutation.PossibleAnswersCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: false,
			Table:   poll.PossibleAnswersTable,
			Columns: poll.PossibleAnswersPrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollanswer.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := puo.mutation.RemovedPossibleAnswersIDs(); len(nodes) > 0 && !puo.mutation.PossibleAnswersCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: false,
			Table:   poll.PossibleAnswersTable,
			Columns: poll.PossibleAnswersPrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollanswer.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := puo.mutation.PossibleAnswersIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: false,
			Table:   poll.PossibleAnswersTable,
			Columns: poll.PossibleAnswersPrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollanswer.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	if puo.mutation.StudentAnswersCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2M,
			Inverse: false,
			Table:   poll.StudentAnswersTable,
			Columns: []string{poll.StudentAnswersColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollstudentanswer.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := puo.mutation.RemovedStudentAnswersIDs(); len(nodes) > 0 && !puo.mutation.StudentAnswersCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2M,
			Inverse: false,
			Table:   poll.StudentAnswersTable,
			Columns: []string{poll.StudentAnswersColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollstudentanswer.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := puo.mutation.StudentAnswersIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2M,
			Inverse: false,
			Table:   poll.StudentAnswersTable,
			Columns: []string{poll.StudentAnswersColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollstudentanswer.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	_node = &Poll{config: puo.config}
	_spec.Assign = _node.assignValues
	_spec.ScanValues = _node.scanValues
	if err = sqlgraph.UpdateNode(ctx, puo.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{poll.Label}
		} else if cerr, ok := isSQLConstraintError(err); ok {
			err = cerr
		}
		return nil, err
	}
	return _node, nil
}
