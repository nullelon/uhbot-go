package main

import (
	"context"
	"encoding/json"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"io"
	"math/rand"
	"os"
	"time"
	"uhbot/internal/app"
	"uhbot/internal/app/domain"
	"uhbot/internal/app/textgen"
	"uhbot/internal/ent"
)

func init() {
	rand.Seed(time.Now().Unix())
}

func main() {
	if len(os.Args) != 3 {
		fmt.Println("В аргументе должен быть токен бота и ключ к розкладу!")
		return
	}

	tgToken := os.Args[1]
	rozkladKey := os.Args[2]

	client, err := openDB()
	if err != nil {
		panic(err)
	}

	//err = loadStudents(client)
	//if err != nil {
	//	panic(err)
	//}

	err = textgen.LoadModelsFrom("./models/")
	if err != nil {
		panic(err)
	}

	rozklad := domain.NewRozklad(rozkladKey)
	links := domain.NewLinks(client)
	polls := domain.NewPolls(client)
	students := domain.NewStudents(client)
	queue := domain.NewQueue(client)

	bot, err := app.NewBot(tgToken, rozklad, links, polls, students, queue)
	if err != nil {
		panic(err)
	}
	bot.Start()
}

func openDB() (*ent.Client, error) {
	client, err := ent.Open("sqlite3", "file:db-data/db.sql?cache=shared&_fk=1")
	if err != nil {
		return nil, fmt.Errorf("failed opening connection to sqlite: %v", err)
	}
	if err := client.Schema.Create(context.Background()); err != nil {
		return nil, fmt.Errorf("failed creating schema resources: %v", err)
	}
	return client, nil
}

type student struct {
	TelegramId int    `json:"telegram_id"`
	Name       string `json:"name"`
}

func loadStudents(ent *ent.Client) error {
	var students []student

	open, err := os.Open("students.json")
	if err != nil {
		return err
	}

	all, err := io.ReadAll(open)
	if err != nil {
		return err
	}

	err = json.Unmarshal(all, &students)
	if err != nil {
		return err
	}

	for _, student := range students {
		ent.Student.Create().SetName(student.Name).SetTelegramID(student.TelegramId).SaveX(context.Background())
	}
	return nil
}
